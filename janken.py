from lib import is_int, are_ints
from random import randint as rint


def judge_janken(player: int, enemy: int) -> str:
    r = (player - enemy + 3) % 3
    if r == 0:
        return "引き分け"
    if r == 1:
        return "負け"
    if r == 2:
        return "勝ち"


def main():

    hands = {0: "グー", 1: "チョキ", 2: "パー"}

    player_hand = input('0: "グー", 1: "チョキ", 2: "パー": ')
    print()

    # 数値かチェック
    is_int(player_hand)

    # 0から2までの数字かチェック
    if 0 > int(player_hand) > 2:
        print("0から2の間で入力してください。")
        return

    enemy_hand = rint(0, 2)
    print(f"自分の手: {hands[int(player_hand)]}")
    print(f"相手の手: {hands[enemy_hand]}")

    print(judge_janken(int(player_hand), enemy_hand))


if __name__ == "__main__":
    main()
