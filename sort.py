from lib import pseudo_represent_inputs, are_ints


def main():
    sins: list = []
    indices = "a", "b", "c"

    # 3回入力を受け付ける
    for _ in range(3):
        sins.append(input())

    # 値が数値かチェック
    are_ints(sins)

    # 擬似的に input の表現
    pseudo_represent_inputs(sins, indices)

    # リストの組み込み関数の sort() を使ってソートする
    sins.sort()
    print("a <= b <= c となるようにソートしました。")
    for i in sins:
        print(i)


if __name__ == "__main__":
    main()
