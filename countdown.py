from lib import is_int


def main():
    print("カウントダウンします。")
    sin = input("正の整数値: ")
    is_int(sin)

    num = int(sin)

    while num >= 0:
        print(num)
        num -= 1


if __name__ == "__main__":
    main()
