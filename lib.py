def is_int(s: str):
    try:
        int(s)
    except ValueError:
        print(f"ValueError: {s} は整数ではありません。")
        return


def are_ints(ls: iter):
    for i in ls:
        is_int(i)


def pseudo_represent_inputs(values, indices, msg="整数値", sep=": ", endl="\n"):
    for v, i in zip(values, indices):
        print(f"{msg}{i}{sep}{v}", end=endl)
