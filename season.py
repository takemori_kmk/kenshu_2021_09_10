from random import randint as rint

from lib import are_ints, is_int, pseudo_represent_inputs


def evaluate_score(score: int) -> str:
    if 0 > score or score >= 100:
        return "不正な点数です。"
    if score >= 80:
        return "優"
    if score >= 70:
        return "良"
    if score >= 60:
        return "可"
    else:
        return "不可"


def main():
    season_map = {
        1: "冬",
        2: "冬",
        3: "春",
        4: "春",
        5: "春",
        6: "夏",
        7: "夏",
        8: "夏",
        9: "秋",
        10: "秋",
        11: "秋",
        12: "冬",
    }
    sin = input("何月ですか: ")

    # check if it is a number
    is_int(sin)
    month = int(sin)
    print(month)

    # easter egg
    if month == 13:
        print("太陰暦には対応していません。")
        return

    # check if it between 1 to 12
    if not (1 <= month <= 12):
        print("1 から 12 の数字を入力してください。")
        return

    print(season_map[month])


if __name__ == "__main__":
    main()
